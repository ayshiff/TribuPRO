//
//  CustomSegment.swift
//  TribuPRO
//
//  Created by Can ATAC on 11/02/2019.
//  Copyright © 2019 Can ATAC. All rights reserved.
//

import UIKit

class CustomSegment: UIView {

        let daysSegmentControl: UISegmentedControl = {
            let control = UISegmentedControl()
            control.backgroundColor = .clear
            control.tintColor = .clear
            control.setTitleTextAttributes([
                NSAttributedString.Key.font: UIFont(name: "DINCondensed-Bold", size: 18) as Any,
                NSAttributedString.Key.foregroundColor: UIColor(red:0.36, green:0.38, blue:0.44, alpha:1)
                ], for: .normal)
            control.setTitleTextAttributes([
                NSAttributedString.Key.font: UIFont(name: "DINCondensed-Bold", size: 18) as Any,
                NSAttributedString.Key.foregroundColor: UIColor(red:0.12, green:0.49, blue:0.86, alpha:1)
                ], for: .selected)
            control.translatesAutoresizingMaskIntoConstraints = false
            return control
        }()
        
        let bottomBar: UIView = {
            let view = UIView()
            view.backgroundColor = UIColor(red:0.12, green:0.49, blue:0.86, alpha:1)
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        
        var segmentItems: [String] = ["7 days", "30 days", "90 days", "1 year"] {
            didSet {
                guard segmentItems.count > 0 else { return }
                setupSegmentItems()
                bottomBarWidthAnchor?.isActive = false
                bottomBarWidthAnchor = bottomBar.widthAnchor.constraint(equalTo: daysSegmentControl.widthAnchor, multiplier: 1 / CGFloat(segmentItems.count))
                bottomBarWidthAnchor?.isActive = true
            }
        }
        
        var bottomBarWidthAnchor: NSLayoutConstraint?
        
        override init(frame: CGRect) {
            super.init(frame: frame)
            setup()
        }
        
        required init?(coder aDecoder: NSCoder) {
            super.init(coder: aDecoder)
            setup()
        }
        
        private func setup() {
            addSubview(daysSegmentControl)
            addSubview(bottomBar)
            
            daysSegmentControl.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
            daysSegmentControl.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
            daysSegmentControl.topAnchor.constraint(equalTo: topAnchor).isActive = true
            daysSegmentControl.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            daysSegmentControl.addTarget(self, action: #selector(segmentedControlValueChanged(_:)), for: .valueChanged)
            
            bottomBar.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
            bottomBar.heightAnchor.constraint(equalToConstant: 2).isActive = true
            bottomBar.leftAnchor.constraint(equalTo: daysSegmentControl.leftAnchor).isActive = true
            bottomBarWidthAnchor = bottomBar.widthAnchor.constraint(equalTo: daysSegmentControl.widthAnchor, multiplier: 1 / CGFloat(segmentItems.count))
            bottomBarWidthAnchor?.isActive = true
            
            setupSegmentItems()
        }
        
        private func setupSegmentItems() {
            for (index, value) in segmentItems.enumerated() {
                daysSegmentControl.insertSegment(withTitle: value, at: index, animated: true)
            }
            daysSegmentControl.selectedSegmentIndex = 0
        }
        
        @objc func segmentedControlValueChanged(_ sender: UISegmentedControl) {
            UIView.animate(withDuration: 0.3) {
                let originX = (self.daysSegmentControl.frame.width / CGFloat(self.segmentItems.count)) * CGFloat(self.daysSegmentControl.selectedSegmentIndex)
                self.bottomBar.frame.origin.x = originX
            }
        }
    
}
