//
//  APIFaceRecognition.swift
//  Tribu
//
//  Created by Rémi Doreau on 07/03/2019.
//  Copyright © 2019 BRED. All rights reserved.
//


import Foundation
import UIKit
import SwiftyJSON

import Alamofire

let APIKey = "" // Ocp-Apim-Subscription-Key
let Region = "francecentral"

// API face endpoints
let DetectUrl = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/detect"
let APIPersonGroups = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/persongroups/"
let Identify = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/identify"

class APIFaceRecognition: APIFaceProtocol {

    /**
     *  PersonGroup
     */

    // Function used to create a new group of persons
    
    func createGroup(groupName: String, name: String, completion: @escaping (Int) -> () /* , userData: String */ ) -> Void {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        let parameters: [String: String] = [
            "name" : name,
            //            "userData" : userData,
        ]

        Alamofire.request("\(APIPersonGroups)\(groupName)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                completion((response.response?.statusCode)!)
        }
    }

    // Function used to add a new person to an existing group
    
    func addPersonToGroup(personGroupId: String, name: String, completion: @escaping (String) -> () /* , userData: String */  ) -> Void {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        let parameters: [String: String] = [
            "name" : name,
            // "userData" : userData,
        ]

        Alamofire.request("\(APIPersonGroups)\(personGroupId)/persons", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                let result = JSON(response.result.value ?? "")
                completion(result["personId"].string!)
        }
    }


    // Function used to add a new person to an existing group
    
    func addFaceToPerson(image: UIImage, personGroupId: String, personId: String, completion: @escaping (String) -> () ) -> Void {

        let headers: HTTPHeaders = [
            "Content-Type": "application/octet-stream",
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        let imageData = image.jpegData(compressionQuality: 0.7)

        Alamofire.upload(imageData!, to: "\(APIPersonGroups)\(personGroupId)/persons/\(personId)/persistedFaces", method: .post, headers: headers)
            .responseJSON { response in
                let result = JSON(response.result.value ?? "")
                completion(result["persistedFaceId"].string!)
        }
    }

    // Function used to train the model
    
    func train(personGroupId: String, completion: @escaping (Int) -> () ) -> Void {
        let headers: HTTPHeaders = [
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        Alamofire.request("\(APIPersonGroups)\(personGroupId)/train", method: .post, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                completion((response.response?.statusCode)!)
        }
    }

    /**
     *  Face
     */

    // Detect human faces in an image, return face rectangles, and optionally with faceIds, landmarks, and attributes.
    
    func detectFaces(image: UIImage, completion: @escaping (String) -> ()) -> Void {
        let headers: HTTPHeaders = [
            "Content-Type": "application/octet-stream",
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        let imageData = image.jpegData(compressionQuality: 0.7)

        Alamofire.upload(imageData!, to: DetectUrl, method: .post, headers: headers)
            .responseJSON { response in

                guard response.result.isSuccess else {
                    print("Error while fetching remote data")
                        completion("ERROR")
                    return
                }
                guard let value = response.result.value as? [[String: Any]] else {
                        print("Malformed data received from service")
                        completion("ERROR")
                        return
                }
                completion(JSON(value)[0]["faceId"].string!)
        }
    }

    // Function used to identify a person given a personGroupId
    
    func identifyPerson(faceIds: [String], personGroupId: String, completion: @escaping (JSON) -> () ) -> Void {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        let parameters: [String: Any] = [
            "faceIds" : faceIds,
            "personGroupId": personGroupId
        ]

        Alamofire.request(Identify, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                let value = JSON(response.result.value ?? "")
                completion(value)
        }
    }
    
    // Function used to get detected user data
    
    func retrievePerson(personId: String, personGroupId: String, completion: @escaping (JSON) -> () ) -> Void {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": APIKey
        ]
        
        Alamofire.request("\(APIPersonGroups)\(personGroupId)/persons/\(personId)", method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                let result = JSON(response.result.value ?? "")
                completion(result)
        }
    }
}

