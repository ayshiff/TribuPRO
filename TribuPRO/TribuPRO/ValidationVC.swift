//
//  ValidationVC.swift
//  TribuPRO
//
//  Created by Can ATAC on 12/02/2019.
//  Copyright © 2019 Can ATAC. All rights reserved.
//

import UIKit

class ValidationVC: UIViewController {

    @IBOutlet weak var identifiedPerson: UILabel!
    @IBAction func backToDashboard(_ sender: UIButton) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: "Initial") as UIViewController
        self.present(vc, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
