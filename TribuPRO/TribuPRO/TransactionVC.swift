//
//  TransactionVC.swift
//  TribuPRO
//
//  Created by Can ATAC on 12/02/2019.
//  Copyright © 2019 Can ATAC. All rights reserved.
//

import UIKit

class TransactionVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var totalAmount: UITextField! {
        didSet {
            totalAmount?.addDoneCancelToolbar(onDone: (target: self, action: #selector(doneButtonTappedForTotalAmount)))
        }
    }

    @objc func doneButtonTappedForTotalAmount() {
        totalAmount.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.totalAmount.delegate = self
        self.totalAmount.becomeFirstResponder()
    }

    
    func textFieldDidEndEditing(_ textField: UITextField) -> Bool{
        self.view.endEditing(true)
        return false
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)

        return false;
    }
}

extension UITextField {
    func addDoneCancelToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onCancel = onCancel ?? (target: self, action: #selector(cancelButtonTapped))
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            UIBarButtonItem(title: "Cancel", style: .plain, target: onCancel.target, action: onCancel.action),
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: .done, target: onDone.target, action: onDone.action)
        ]
        toolbar.sizeToFit()
        
        self.inputAccessoryView = toolbar
    }
    
    // Default actions:
    @objc func doneButtonTapped() { self.resignFirstResponder() }
    @objc func cancelButtonTapped() { self.resignFirstResponder() }
}
