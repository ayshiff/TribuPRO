//
//  PhotoVC.swift
//  TribuPRO
//
//  Created by Can ATAC on 13/02/2019.
//  Copyright © 2019 Can ATAC. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class PhotoVC: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    let apiface = APIFaceRecognition()

    var cameraView = UIView()

    let captureSession = AVCaptureSession()
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var pictureFrameView:UIView?
    var stillImageOutput:AVCaptureStillImageOutput = AVCaptureStillImageOutput()
    var image:UIImage                   =   UIImage()
    var imageData:NSData                =   NSData()
    var idCardScanning:Bool             =   false

    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr,
                                      AVMetadataObject.ObjectType.face]
    
    @IBAction func takePhoto(_ sender: Any) {
        beginScan()
    }
    func beginScan(){
        
        if let videoConnection = stillImageOutput.connection(with: AVMediaType.video) {
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                self.imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer!) as! NSData
                
                self.image   =   UIImage(data: self.imageData as Data)!
                self.image   =   self.resize(image: self.image)
                
                self.identifyPerson(personGroupId: "members", image: self.image)
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        captureSession.sessionPreset = AVCaptureSession.Preset.vga640x480
        
        let devices = AVCaptureDevice.devices()
        // Loop through all the capture devices on this phone
        for device in devices {
            // Make sure this particular device supports video
            if (device.hasMediaType(AVMediaType.video)) {
                // Finally check the position and confirm we've got the back camera
                if(device.position == AVCaptureDevice.Position.back) {
                    captureDevice = device
                }
            }
        }
        
        if captureDevice != nil {
            beginSession()
        }
        
    }
    
    //* cf.http://www.howtobuildsoftware.com/index.php/how-do/SB0/ios-swift-uiview-front-camera-to-fill-circular-uiview
    
    func beginSession() {
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
        cameraView.frame = CGRectMake(10, 100, 300, 300)
        cameraView.backgroundColor = UIColor(red:26/255, green:188/255, blue:156/255, alpha:1)
        let tribuColor = UIColor(red:0/255, green:216/255, blue:254/255, alpha:1)
        cameraView.layer.cornerRadius = (cameraView.frame.size.width) / 2
        cameraView.layer.borderColor = tribuColor.cgColor
        cameraView.layer.borderWidth = 2
        cameraView.contentMode = UIView.ContentMode.scaleToFill
        cameraView.layer.masksToBounds = true
        
        view.addSubview(cameraView)
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        var rootLayer :CALayer = cameraView.layer
        rootLayer.masksToBounds=true
        videoPreviewLayer!.frame = rootLayer.bounds
        rootLayer.addSublayer(videoPreviewLayer!)
        // cf.iOS 10 Book p.836
        let outputSettings:NSDictionary =   NSDictionary(object: AVVideoCodecJPEG, forKey: AVVideoCodecKey as NSCopying)
        stillImageOutput.outputSettings =   outputSettings as! [String : Any]
        captureSession.addOutput(stillImageOutput)
        //
        captureSession.startRunning()
    }

    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }

    func resize(image: UIImage)->UIImage{
        var actualHeight:CGFloat            =   image.size.height
        var actualWidth:CGFloat             =   image.size.width
        
        var maxHeight:CGFloat               =   0.0//idScan?300.0:1224.0
        var maxWidth:CGFloat                =   0.0//idScan?400.0:1224.0
        
        if self.idCardScanning{maxHeight    =   300.0}else{maxHeight    =   1224.0}
        if self.idCardScanning{maxWidth     =   400.0}else{maxWidth     =   1224.0}
        
        var imgRatio:CGFloat                =   actualWidth/actualHeight;
        let maxRatio:CGFloat                =   maxWidth/maxHeight;
        var compressionQuality:CGFloat      =   0.0
        if self.idCardScanning{compressionQuality = 0.5}else{compressionQuality = 0.0}
        
        if (actualHeight > maxHeight || actualWidth > maxWidth)
        {
            if(imgRatio < maxRatio)
            {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio)
            {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else
            {
                actualHeight = maxHeight;
                actualWidth = maxWidth;
            }
        }
        
        let rect:CGRect                     =   CGRect(x: 0.0, y: 0.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img                             =   UIGraphicsGetImageFromCurrentImageContext()
        let imageData                       =   img!.jpegData(compressionQuality: compressionQuality)
        
        UIGraphicsEndImageContext()
        
        return UIImage(data: imageData!)!
    }
    
    
    // Identify a person
    func identifyPerson(personGroupId: String, image: UIImage) {
        self.apiface.detectFaces(image: image) { faceIdResult in
            guard faceIdResult != "ERROR" else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "USER_NOT_FOUND"), object: "USER NOT FOUND !", userInfo: ["name":"Photographiez un visage"])
                return
            }
            let faceId = faceIdResult
            self.apiface.identifyPerson(faceIds: [faceId], personGroupId: personGroupId) { identifiedPersonResult in
                let identifiedPerson = identifiedPersonResult
                
                if identifiedPerson[0]["candidates"].isEmpty == false {
                    let personId = identifiedPerson[0]["candidates"][0]["personId"].string!
                    self.apiface.retrievePerson(personId: personId, personGroupId: personGroupId) { retrievedPerson in
                        print(retrievedPerson)
                        let name = retrievedPerson["name"].string!
                        NotificationCenter.default.post(name: Notification.Name("GOT_A_PERSON"), object: nil, userInfo: ["name": name, "photo":image])
                    }
                } else {
                    print("User not found !")
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "USER_NOT_FOUND"), object: "USER NOT FOUND !", userInfo: ["name":"Membre inconnu"])
                }
            }
        }
    }
}

