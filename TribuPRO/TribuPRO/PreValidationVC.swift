//
//  PreValidationVC.swift
//  TribuPRO
//
//  Created by Can ATAC on 21/03/2019.
//  Copyright © 2019 Can ATAC. All rights reserved.
//

import UIKit

class PreValidationVC: UIViewController {

    @IBOutlet weak var response: UILabel!
    @IBOutlet weak var userPhoto: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showPerson), name: Notification.Name("GOT_A_PERSON"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.showError), name: Notification.Name("USER_NOT_FOUND"), object: nil)
        // Do any additional setup after loading the view.
    }
    
    @objc func showError(notification: Notification){
        let welcomeText = "CLIENT : " + (notification.userInfo?["name"] as! String)
        self.response.text = welcomeText
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("USER_NOT_FOUND"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("GOT_A_PERSON"), object: nil)

    }
    @objc func showPerson(notification: Notification){
        self.response.text = notification.userInfo?["name"] as! String
        self.userPhoto.image = notification.userInfo?["photo"] as! UIImage
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name("GOT_A_PERSON"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name("USER_NOT_FOUND"), object: nil)

    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
